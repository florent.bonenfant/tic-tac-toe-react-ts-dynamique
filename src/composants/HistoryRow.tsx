import * as React from 'react';

export interface interfaceHistoryRow {
    move: number;
    coords: number[];
    onClickHistoryRow?: any;
    current: boolean;
}

export const HistoryRow : React.FC<interfaceHistoryRow> = ({ move, coords, onClickHistoryRow, current }) => {

    return (
        <li>
            <button 
                onClick={onClickHistoryRow}
                value={move}
                className={(current ? "bold" : "")} 
            >{'Mouvement ' + move + ' - (' + coords + ')'}</button>
        </li>
    );
}

export default HistoryRow;