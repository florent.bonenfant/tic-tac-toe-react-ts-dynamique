// import * as React from 'react';
import React, { useState } from "react";
import { Board } from "./Board";
import { History } from "./History";
import "./style.css";

export interface interfaceGame {
  sizeBoard: number[];
  numberOfPlayers: number;
  victoryTo: number;
}

export const Game: React.FC<interfaceGame> = ({
  sizeBoard = [3, 3],
  numberOfPlayers = 2,
  victoryTo = 2
}) => {
  if (numberOfPlayers < 2) {
    numberOfPlayers = 2;
    console.info("Le nombre minimum de joueur est 2");
  }
  if (numberOfPlayers > 5) {
    numberOfPlayers = 5;
    console.info("Le nombre maximum de joueur est 5");
  }

  /**
   * Création du tableau multidimensionnel pour la grille de jeu
   * @param sizeBoard
   * @return array
   */
  const newGrid = (sizeBoard: number[]): string[][] => {
    var logBoard = new Array(sizeBoard[0]);
    for (let y = 0; y < sizeBoard[0]; y++) {
      logBoard[y] = new Array(sizeBoard[1]);
      for (let x = 0; x < sizeBoard[1]; x++) {
        logBoard[y][x] = null;
      }
    }
    return logBoard;
  };


  /**
   * Gestion des cases cochés et vérification du résultat pour les lignes et les colonnes
   * @param player 
   * @param x coordonnées
   * @param y coordonnées
   * @param casesGagnantes array
   */
  const checkWinnerByLine = (player: string, x: number, y: number, casesGagnantes: any[]): any => {
    if (stateGrid[y] !== undefined && stateGrid[y][x] !== undefined) {
      if (stateGrid[y][x] === player) {
        if (casesGagnantes.length < victoryTo) {
          casesGagnantes = addCaseWinner(casesGagnantes, x, y);

          // Vérification résultat H & V
          for (let c = 0; c < casesGagnantes.length; c++) {
            if (
              casesGagnantes[0][0] !== casesGagnantes[c][0] &&
              casesGagnantes[0][1] !== casesGagnantes[c][1]
            ) {
              return casesGagnantes = [];
            }
          }

        }
      } else {
        if (casesGagnantes.length < victoryTo) {
          casesGagnantes = [];
        }
      }
    }

    return casesGagnantes;
  };



  /**
   * Gestion des cases cochés et vérification du résultat pour les diagonales
   * @param player 
   * @param x coordonnées
   * @param y coordonnées
   * @param casesGagnantes array
   */
  const checkWinnerByDiag = (x: number, y: number, casesGagnantes:any[]): any => {

    if (casesGagnantes.length < victoryTo) {
      casesGagnantes = [];
      // Vérification à droite
        for (let c = 0; c < victoryTo; c++) {
          if (
            stateGrid[y + c] !== undefined &&
            stateGrid[y + c][x + c] !== undefined &&
            stateGrid[y + c][x + c] === stateGrid[y][x]
          ) {
            casesGagnantes = addCaseWinner(casesGagnantes, x + c , y + c);
          }
        }

        if (casesGagnantes.length >= victoryTo) {
          return casesGagnantes;
        }

        casesGagnantes = [];
        for (let c = 0; c < victoryTo; c++) {
          if (
            stateGrid[y + c] !== undefined &&
            stateGrid[y + c][x - c] !== undefined &&
            stateGrid[y + c][x - c] === stateGrid[y][x]
          ) {
            casesGagnantes = addCaseWinner(casesGagnantes, x - c , y + c);
          }
        }
    }

    return casesGagnantes;
  };


/**
 * Vérifie si un joueur à gagné
 * @param coords array coordonnées
 */
  const checkWinner = (coords: number[]): any => {
    let player: string = stateGrid[coords[0]][coords[1]];
    let casesGagnantes: any[] = [];
    let casesGagnantesH: any[] = [];
    let casesGagnantesD: any[] = [];

    stateWinner.shift();

    let casesAVerifier: number = victoryTo - 1;
    // On réduit la taille du tableau à crawler autour de la case cliqué
    let minStartCrawlX:number = (coords[1] - casesAVerifier >= 0 ? coords[1] - casesAVerifier : 0);
    let minStartCrawlY:number = (coords[0] - casesAVerifier >= 0 ? coords[0] - casesAVerifier : 0);
    let maxStartCrawlX:number = (coords[1] + casesAVerifier <= sizeBoard[0] - 1 ? coords[1] + casesAVerifier : sizeBoard[0] - 1);
    let maxStartCrawlY:number = (coords[0] + casesAVerifier <= sizeBoard[1] - 1 ? coords[0] + casesAVerifier : sizeBoard[1] - 1);


    for (let y = minStartCrawlY; y <= maxStartCrawlY; y++) {
      for (let x = minStartCrawlX; x <= maxStartCrawlX; x++) {

        if (player === stateGrid[y][x]) {
          // H
          casesGagnantesH = JSON.parse(JSON.stringify(checkWinnerByLine(player, x, y, casesGagnantesH)));

          // D
          casesGagnantesD = JSON.parse(JSON.stringify(checkWinnerByDiag(x, y, casesGagnantesD)));
        }

      }
    }

    if (
      casesGagnantesH.length >= victoryTo ||
      casesGagnantesD.length >= victoryTo
    ) {
      if (casesGagnantesH.length >= victoryTo ) {
        casesGagnantes = casesGagnantesH;
      }
      if (casesGagnantesD.length >= victoryTo ) {
        casesGagnantes = casesGagnantesD;
      }

      // victoire
      console.info("VICTOIRE ... de " + player);
      setStateWinner(casesGagnantes);
      return casesGagnantes;
    }

    return null;
  };

  /**
   * Ajoute une nouvelle case au tableau gagnant
   * @param x 
   * @param y 
   */
  const addCaseWinner = (listCell : any[], x: number, y : number): any => {
    let exist = false;

    // Vérification doublon
    listCell.forEach(element => {
      if (element[0] === x && element[1] === y) {
        exist = true;
      }
    });

    if (!exist) {
      listCell.push([x, y]);
    }
    return listCell;
  }

  // Gestion de l'état local
  const [stateGrid, setStateGrid] = useState(newGrid(sizeBoard));
  const [stateMove, setStateMove] = useState(0);
  const [statePlayer, setStatePlayer] = useState(0);
  const [stateWinner, setStateWinner] = useState([[-1, -1]]);
  const [stateHistory, setStateHistory] = useState([
    {
      // Définition des propriétés pour ne pas être bloqué lorsqu'on veut les utilisés...
      grid: newGrid(sizeBoard),
      lastMove: [0, 0],
      move: stateMove,
      player: statePlayer
    }
  ]);

  const players: string[] = ["X", "O", "@", "#", "¤"];

  /**
   * Gestion du clique sur une cellule
   */
  const handleClickCell = (coords: number[]) => {

    // Case déjà joué
    if (stateGrid[coords[0]][coords[1]] !== null) {
      alert("La case à déjà était joué");
      return;
    }

    if (stateWinner.length >= victoryTo) {
      console.info('fin de partie')
      return;
    }

    // on supprime l'historique écrasé
    if (stateMove < stateHistory.length - 1) {
      stateHistory.splice(Number(stateMove) + 1);
    }

    // Gestion des états locaux
    stateGrid[coords[0]][coords[1]] = players[statePlayer];

    setStateMove(Number(stateMove) + 1);
    if (numberOfPlayers > statePlayer + 1) {
      setStatePlayer(statePlayer + 1);
    } else {
      setStatePlayer(0);
    }

    setStateHistory(
      stateHistory.concat({
        move: Number(stateHistory.length),
        // Après plusieurs heures de recherche on découvre la magie des tableaux multidimensionnels
        grid: JSON.parse(JSON.stringify(stateGrid)),
        lastMove: coords,
        player: statePlayer
      })
    );

    checkWinner(coords);
  };

  /**
   * Gestion du clique sur l'historique
   * @param mouvement
   */
  const handleClickHistoryRow = (move: number) => {
    if (stateWinner.length >= victoryTo) {
      alert('Genre t\'aime pas perdre ?');
      return;
    }

    let histo = stateHistory[Number(move)];
    setStateGrid(JSON.parse(JSON.stringify(histo.grid)));
    if (numberOfPlayers > histo.player + 1) {
      setStatePlayer(histo.player + 1);
    } else {
      setStatePlayer(0);
    }

    setStateMove(move);

    setStateWinner([]);
  };

  const getStatusOfGame = () => {
    return "Prochain joueur : " + players[statePlayer];
  };

  return (
    <div className="game">
      <div className="game-board">
        <Board
          numberOfPlayers={numberOfPlayers}
          onClickCell={(e: any) => handleClickCell(JSON.parse(e.target.value))}
          grid={stateGrid}
          winner={stateWinner}
        />
      </div>
      <div className="game-info">
        <div>{getStatusOfGame()}</div>
        {
          <History
            history={stateHistory}
            current={Number(stateMove)}
            onClickHistoryRow={(e: any) =>
              handleClickHistoryRow(e.target.value)
            }
          />
        }
      </div>
    </div>
  );
};

export default Game;
