import * as React from 'react';

export interface interfaceCell {
    onClickCell?: any;
    cellNum: number;
    rowNum: number;
    grid: string[][];
    winner: any[];
}

export const Cell : React.FC<interfaceCell> = ({ onClickCell, cellNum, rowNum, grid, winner }) => {

    let victoire = false;
    if (winner !== undefined && winner !== null && winner[0] !== -1) {
      for (let w = 0; w < winner.length; w++) {
        if (winner[w][0] === cellNum && winner[w][1] === rowNum) {
            victoire = true;
          break;
        }
      }
    }
    return (
        <button className={victoire ? "square red-back" : "square" }
            onClick={onClickCell}
            value={ JSON.stringify([ rowNum, cellNum ]) }>
            {grid[rowNum][cellNum]}
        </button>
        // <button onClick={props.onClick} className={props.victoire ? "square red-back" : "square" }>
        // {props.value}
        // </button>
    );
}

export default Cell;