import * as React from 'react';
import { Cell } from './Cell';

export interface InterfaceRow {
    rowNum: number;
    onClick?: any;
    onClickCell?: any;
    grid: string[][];
    winner: any[];
}


export const Row : React.FC<InterfaceRow> = ({ onClickCell, rowNum, grid, onClick, winner }) => {

    let render: any[] = Array(grid[rowNum].length);

    for (let i=0; i < grid[rowNum].length; i++) {
        render.push(<Cell key={i} cellNum={i} rowNum={rowNum}  grid={grid} winner={winner} />);
    }

    return (
        <div className="board-row" onClick={onClickCell} >{render}</div>
    );
}

export default Row;