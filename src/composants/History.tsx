import * as React from 'react';
import { HistoryRow } from './HistoryRow';

export interface interfaceHistory {
    history: any[];
    onClickHistoryRow?: any;
    current: number;
}

export const History : React.FC<interfaceHistory> = ({ history = [], onClickHistoryRow, current }) => {

    let render: any[] = Array(history.length);

    for (let i=0; i < history.length; i++) {
        render.push(<HistoryRow key={i} coords={history[i].lastMove} move={history[i].move} current={current === history[i].move} />);
    }
    return (
        <ol onClick={onClickHistoryRow}>
            {render}
        </ol>
    );
}

export default History;