import * as React from 'react';
// import React, { useState, useContext, useEffect } from "react";
import { Row } from './Row';

export interface InterfaceBoard {
    numberOfPlayers: number;
    onClickCell?: any;
    grid: string[][];
    winner: any[];
}




export const Board : React.FC<InterfaceBoard> = ({ onClickCell, grid, numberOfPlayers = 2, winner }) => {

    // let render: any[] = Array(sizeBoard[0]).fill(<Row sizeBoard={sizeBoard} onClickCell={(e:any) => handleClickCell(e.target)}/>);
    let render: any[] = Array(grid.length);

    for (let i=0; i < grid.length; i++) {
        render.push(
            <Row onClickCell={onClickCell} rowNum={i} key={i} grid={grid} winner={winner} />
        );
    }

    return (
        // React.Fragment permet de ne pas ouvrir de balise inutile
        <React.Fragment>
            {render}
        </React.Fragment>
    );
}

export default Board;