import React from 'react';
import ReactDOM from 'react-dom';
import { Game } from './composants/Game';

import './index.css';

ReactDOM.render(<Game sizeBoard={[5, 5]} numberOfPlayers={3} victoryTo={4} />, document.getElementById('root'));
